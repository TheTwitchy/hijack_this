# Hijack This

Linux traffic interception scripts and helpers. Included components:
* hijack_eth2 - A layer 2 interception script. Turns your intercepting host into a 2 port switch. NOT CURRENTLY FUNCTIONAL
* hijack_eth3 - A layer 3 interception script. Turns your intercepting host into a standard SOHO router. Recommended for most wired setups.
* hijack_wifi - A 802.11 interception script. Turns your intercepting host into a standard SOHO router with with WiFi. Recommended for most wireless setups.
* hijack_tcp - A simple TCP relay script. Listens on one port and sends traffic elsewhere after changing it. Sometimes useful, normally would just go for hijack_nb
* hijack_nb - A nonblocking TCP relay script. Listens on on one port and forwards to another host after a regex sub. Supports multiple connections at once. Useful for non-HTTP connections.
* enable_forwarding - Quick way to enable traffic forwarding in your kernel.