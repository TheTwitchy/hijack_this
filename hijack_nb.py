#!/usr/bin/env python

# hijack_nb
# This is a multithreaded TCP relay server that uses nonblocking sockets.
# What this means in laymans terms is that it sits in between another client
# and server, and just relays them back and forth. Obviously there's the
# capability to modify the traffic on the fly as well. The thing that separates
# this from a couple of netcat listeners piped together is that this will
# allow multiple simultaneous connections to the same host:port (like a real
# server), and it will asynchronously handle the recv buffers from any source.
# Getting the traffic to this intercepting server is an exercise left to the
# reader (hint: use iptables).

##
# Requirements:
#  * pip install Twisted pyopenssl termcolor


# Application imports.
import sys
import random
import string
import re
# import termcolor
import argparse
from twisted.internet import reactor, protocol, ssl
from OpenSSL import crypto


# Application global vars
VERSION = "0.1"
PROG_NAME = "hijack_nonblock.py"
PROG_DESC = "A nonblocking, multithreaded TCP relay"
PROG_EPILOG = "Written by TheTwitchy."
DEBUG = True


def try_color(string, color):
    return string
    # return termcolor.colored(string, color)


# Print some info to stdout
def print_info(*args):
    sys.stdout.write(try_color("info: ", "green"))
    sys.stdout.write(try_color(" ".join(map(str, args)) + "\n", "green"))


# Print an error to stderr
def print_err(*args):
    sys.stderr.write(try_color("error: ", "red"))
    sys.stderr.write(try_color(" ".join(map(str, args)) + "\n", "red"))


# Print a debug statement to stdout
def print_debug(*args):
    if DEBUG:
        sys.stderr.write(try_color("debug: ", "blue"))
        sys.stderr.write(try_color(" ".join(map(str, args)) + "\n", "blue"))


def hostport_type1(s, pat=re.compile(r"[a-zA-Z\d\-\.]+:*\d*")):
    if not pat.match(s):
        raise argparse.ArgumentTypeError
    return s


def hostport_type2(s, pat=re.compile(r"[a-zA-Z\d\-\.]*:*\d+")):
    if not pat.match(s):
        raise argparse.ArgumentTypeError
    return s


# Because.
def print_header():
    print("                                     ")
    print(" _   _   _         _             _   ")
    print("| |_|_| |_|___ ___| |_       ___| |_ ")
    print("|   | | | | .'|  _| '_|     |   | . |")
    print("|_|_|_|_| |__,|___|_,_|_____|_|_|___|")
    print("      |___|           |_____|        ")
    print("                             v" + VERSION)
    print("")


# Argument parsing which outputs a dictionary.
def parseArgs():
    # Setup the argparser and all args
    parser = argparse.ArgumentParser(prog=PROG_NAME,
                                     description=PROG_DESC,
                                     epilog=PROG_EPILOG)
    parser.add_argument("-v", "--version",
                        action="version",
                        version="%(prog)s " + VERSION)
    parser.add_argument("-q", "--quiet",
                        help="surpress extra output",
                        action="store_true",
                        default=False)
    parser.add_argument("-l", "--local",
                        help="local listening host address:port",
                        type=hostport_type2,
                        required=True)
    parser.add_argument("-r", "--remote",
                        help="remote relaying host address:port",
                        required=True,
                        type=hostport_type1)
    parser.add_argument("-s", "--ssl",
                        help="use SSL/TLS for connections (include " +
                        "the server PEM)",
                        default=None,
                        required=False,
                        type=argparse.FileType("r"))
    parser.add_argument("-c", "--certs",
                        help="use SSL/TLS for connections (include the " +
                        "server PEM cert chain)",
                        default=None,
                        required=False,
                        type=argparse.FileType("r"))
    parser.add_argument("regex", help="regex to match", nargs="?")
    parser.add_argument("replace", help="replacement string", nargs="?")

    return parser.parse_args()


# This is the object that asynchronously handles connections once they
# complete the connection to the server.
class RelayListener(protocol.Protocol):
    def __init__(self, handle_id, remote_host, remote_port, regex, replace,
                 ssl):
        self.handle_id = handle_id
        self.remote_host = remote_host
        self.remote_port = remote_port
        self.client_factory = None
        if isinstance(regex, str):
            # regex can be a string, but since we want everything to be bytes,
            # we need to convert it.
            regex = regex.encode()
        self.regex = regex
        if isinstance(replace, str):
            # replace can be a string, but since we want everything to be
            # bytes, we need to convert it.
            replace = replace.encode()
        self.replace = re.escape(replace)
        self.ssl = ssl
        self.buffer = None
        self.client = None

    def connectionMade(self):
        self.client_factory = RelayClientFactory(self.handle_id,
                                                 self.transport,
                                                 self.regex,
                                                 self.replace)
        self.client_factory.server = self
        if self.ssl:
            # THIS ACCEPTS LITERALLY ANY SERVER CERTIFICATE
            options = ssl.CertificateOptions(verify=False)
            print_debug("Opening new SSL Client")
            reactor.connectSSL(self.remote_host, self.remote_port,
                               self.client_factory, options)
        else:
            reactor.connectTCP(self.remote_host, self.remote_port,
                               self.client_factory)

        print_info("[" + self.handle_id +
                   "] Local server recvd connection from " +
                   str(self.transport.getPeer().host) + ":" +
                   str(self.transport.getPeer().port))

    def connectionLost(self, reason):
        print_info("[" + self.handle_id + "] Closed connection")

    def dataReceived(self, data):
        print_debug("[" + self.handle_id + "] Local server recvd " +
                    str(len(data)) + " bytes from true client at " +
                    str(self.transport.getPeer().host) + ":" +
                    str(self.transport.getPeer().port))
        print_debug("[" + self.handle_id + "] > " + str(data))

        if self.regex is not None:
            re_obj = re.compile(self.regex)

            # check if data is a string
            if isinstance(data, str):
                # if so, convert it to bytes
                data = data.encode()

            if re_obj.search(data):
                print_info("[" + self.handle_id +
                           "] Local server recv regex match found for " +
                           str(self.regex))
            if self.replace is not None:
                old_data = data
                data = re_obj.sub(self.replace, data)

                if data != old_data:
                    print_debug("[" + self.handle_id + "] Replaced data:")
                    print_debug("[" + self.handle_id + "] > " + str(data))

        if self.client:
            self.client.write(data)
        else:
            if self.buffer is None:
                self.buffer = data
            else:
                print_err("[" + self.handle_id +
                          "] Failed to send data twice, "
                          "buffer is already full")

    def write(self, data):
        self.transport.write(data)


# This object will hold the client factory so that all spawned connections
# made sending sockets from the same object
class RelayListenerFactory(protocol.Factory):
    def __init__(self, remote_host, remote_port, regex, replace, ssl=False):
        self.handles = []
        self.remote_host = remote_host
        self.remote_port = remote_port
        self.regex = regex
        self.replace = replace
        self.ssl = ssl

    def getNewHandle(self):
        tmp_id = ''.join(random.choice("abcdef" + string.digits)
                         for _ in range(4))
        while tmp_id in self.handles:
            tmp_id = ''.join(random.choice("abcdef" + string.digits)
                             for _ in range(4))
        self.handles.append(tmp_id)
        return tmp_id

    def buildProtocol(self, addr):
        return RelayListener(self.getNewHandle(), self.remote_host,
                             self.remote_port, self.regex, self.replace,
                             self.ssl)


# This is the object that asynchronously sends data.
class RelayClient(protocol.Protocol):
    def __init__(self, handle_id, remote_transport, parent_factory, regex,
                 replace):
        self.handle_id = handle_id
        self.remote_transport = remote_transport
        self.factory = parent_factory
        if isinstance(regex, str):
            # regex can be a string, but since we want everything to be bytes,
            # we need to convert it.
            regex = regex.encode()
        self.regex = regex
        if isinstance(replace, str):
            # replace can be a string, but since we want everything to be
            # bytes, we need to convert it.
            replace = replace.encode()
        self.replace = re.escape(replace)

    def connectionMade(self):
        self.factory.server.client = self
        self.write(self.factory.server.buffer)
        self.factory.server.buffer = None
        print_info("[" + self.handle_id +
                   "] Local client opened connection to " +
                   str(self.transport.getPeer().host) + ":" +
                   str(self.transport.getPeer().port))

    def dataReceived(self, data):
        print_debug("[" + self.handle_id + "] Local client recvd " +
                    str(len(data)) + " bytes from true server at " +
                    str(self.transport.getPeer().host) + ":" +
                    str(self.transport.getPeer().port))
        print_debug("[" + self.handle_id + "] < " + str(data))

        if self.regex is not None:
            re_obj = re.compile(self.regex)

            # check if data is a string
            if isinstance(data, str):
                # if so, convert it to bytes
                data = data.encode()

            if re_obj.search(data):
                print_info("[" + self.handle_id +
                           "] Local client recv regex match found for " +
                           str(self.regex))
            if self.replace is not None:
                old_data = data
                data = re_obj.sub(self.replace, data)

                if data != old_data:
                    print_debug("[" + self.handle_id + "] Replaced data:")
                    print_debug("[" + self.handle_id + "] < " + str(data))

        self.factory.server.write(data)

    def write(self, data):
        if data:
            self.transport.write(data)


class RelayClientFactory(protocol.ClientFactory):
    def __init__(self, handle_id, remote_transport, regex, replace):
        self.handle_id = handle_id
        self.remote_transport = remote_transport
        self.transport = None
        self.regex = regex
        self.replace = replace

    def buildProtocol(self, addr):
        return RelayClient(self.handle_id, self.remote_transport, self,
                           self.regex, self.replace)


def parse_host(host_str, default_host=None, default_port=None):
    host = ""
    port = 0
    if ":" in host_str:
        tmp = host_str.split(":")
        try:
            host = tmp[0]
        except Exception:
            if default_host:
                host = default_host
            else:
                print_err("The host '" + host_str + "' is malformed")
                quit(-1)

        try:
            port = int(tmp[1])
        except Exception:
            if default_port:
                host = default_port
            else:
                print_err("The host '" + host_str + "' is malformed")
                quit(-1)
    else:
        if re.match(r"\d*", host_str) and default_host is not None:
            host = default_host
            port = int(host_str)
        elif default_port is not None:
            host = host_str
            port = default_port
        else:
            print_err("The host '" + host_str + "' is malformed")
            quit(-1)
    return (host, port)


# Main application entry point.
def main():
    argv = parseArgs()

    # Print out some sweet ASCII art.
    if not argv.quiet:
        print_header()

    # Begin application logic.
    local_addr, local_port = parse_host(argv.local, default_host="0.0.0.0")
    remote_addr, remote_port = parse_host(argv.remote, default_port=local_port)

    if argv.regex:
        try:
            re.compile(argv.regex)
        except SyntaxError:
            print_err("Bad regex, failed to compile correctly.")
            quit(-1)

    if argv.ssl:
        certificate = ssl.PrivateCertificate.loadPEM(argv.ssl.read())
        listener_factory = RelayListenerFactory(remote_addr, remote_port,
                                                argv.regex, argv.replace, True)

        cert_options = certificate.options()
        if argv.certs:
            print_debug("Setting full certificate chain")
            cert_options.extraCertChain = [crypto.load_certificate(
                crypto.FILETYPE_PEM, argv.certs.read())]

        reactor.listenSSL(local_port, listener_factory, cert_options,
                          interface=local_addr)
        print_info("Starting SSL/TLS relay server. Press Ctrl-C to quit.")
        reactor.run()

    else:
        listener_factory = RelayListenerFactory(remote_addr, remote_port,
                                                argv.regex, argv.replace)
        reactor.listenTCP(local_port, listener_factory, interface=local_addr)
        print_info("Starting relay server. Press Ctrl-C to quit.")
        reactor.run()


if __name__ == "__main__":
    main()
    print("")
    quit(0)
