#!/usr/bin/env python

## A simple passive-relay, from stitch
## ^C to exit
## Requirements: 
##  * pwntools (pip install pwntools)

PORT = 40934
RELAY_TO = "10.0.0.44"

from pwn import *
import re

context.log_level = 'DEBUG'

client = listen(PORT).wait_for_connection()
server = remote(RELAY_TO, PORT)

while 1:
    print '\n[*] FROM CLIENT'
    msg_from_client = client.recv(timeout = 10)
    ## Replace all instances of foo with bar
    msg_from_client = re.sub("foo", "bar", msg_from_client)
    server.send(msg_from_client)

    print '\n[*] FROM SERVER'
    msg_from_server = server.recv(timeout = 10)
    ## Replace all instances of foo with bar
    msg_from_server = re.sub("foo", "bar", msg_from_server)
    client.send(msg_from_server)

